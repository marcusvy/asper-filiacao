"use strict";

var app = angular.module("adesaoApp", [
    'ngRoute',
    'ngSanitize',
    'ngCookies',
    'ngResource'
]);

app.service("DadosService", ['$rootScope', '$filter',
    function ($rootScope, $filter) {
        var service = {

            FormData: {},

            FormDataTitular: {
                titular_nome: null,
                titular_datanasc: null,
                titular_sexo: 'f',
                titular_naturalidade: null,
                titular_situacao: null,
                titular_identidade: null,
                titular_identidade_oe: null,
                titular_cpfmf: null,
                titular_pispasep: null,
                titular_cns: null,
                titular_cargo: null,
                titular_dataadmissao: null,
                titular_secretaria: null,
                titular_matricula: null,
                titular_estadocivil: null,
                titular_end_logradouro: null,
                titular_end_numero: null,
                titular_end_bairro: null,
                titular_end_municipio: null,
                titular_end_cep: null,
                titular_tel_trabalho: null,
                titular_tel_residencia: null,
                titular_celular: null,
                titular_email: null,
                titular_banco_nome: null,
                titular_banco_agencia: null,
                titular_banco_cc: null
            },

            FormDataAgregado: {
                agregado_nome: null,
                agregado_cpfmf: null,
                agregado_sexo: 'f',
                agregado_vinculoparentesco: null,
                agregado_filiacao: null,
                agregado_datanasc: null,
                agregado_cns: null
            },
					
						FormDataBeneficio: {
								titular_ps: false,
								titular_po: false,
								agregado_ps: [null,false,false,false,false,false],
								agregado_po: [null,false,false,false,false,false],
								obs: null
						},

            Agregados: [],

            /**
             * Obtem os dados do Titular cadastrado
             */
            getTitular: function () {
                return this.FormDataTitular;
            },

            /**
             * @param {object} newValue Novo objeto titular
             */
            setTitular: function (newValue) {
                this.FormDataTitular = newValue;
                return $rootScope.$broadcast('DadosService.setTitular');
            },

            /**
             * Obtem o agregado de índice 'index'
             * @param {number} index Índice do Agregado
             */
            getAgregado: function (index) {
                return $filter('filter')(this.Agregados, {
                    index: index
                }, true);
            },

            /**
             * Obtem o agregado de índice 'index'
             * @param {number} index Índice do Agregado
             * @param {object} objAgregado Novo objeto titular
             */
            setAgregado: function (index, objAgregado) {
                angular.forEach(this.Agregados, function (value, key) {
                    if (key === (index - 1)) {
                        value.FormDataAgregado = objAgregado;
                    }
                });
                return $rootScope.$broadcast('DadosService.setAgregado');
            },

            /**
             * Adiciona um agregado a lista.
             * @param {object} objAgregado Novo objeto titular
             */
            addAgregado: function (objAgregado) {
                var indexObj = this.Agregados.length + 1;
                var obj = {
                    index: indexObj,
                    FormDataAgregado: objAgregado
                };
                this.Agregados.push(obj);
                return $rootScope.$broadcast('DadosService.addAgregado');
            },

            /**
             * Obtem lista de Agregados
             */
            getAgregados: function () {
                return this.Agregados;
            },

            /**
             * Altera toda a lista de agregados de uma só vez. (Cuidado!)
             * @param {object} newValue Novo objeto titular
             */
            setAgregados: function (newValue) {
                this.Agregados = newValue;
                return $rootScope.$broadcast('DadosService.setAgregados');
            },

            /**
             * Retorna o total de agregados inseridos
             */
            getTotalAgregados: function () {
                return this.Agregados.length;
            },
						
						/**
             * Obtem os dados do Beneficios cadastrados
             */
            getBeneficio: function () {
                return this.FormDataBeneficio;
            },
						
						/**
             * @param {object} newValue Novo objeto titular
             */
            setBeneficio: function (newValue) {
                this.FormDataBeneficio = newValue;
                return $rootScope.$broadcast('DadosService.setBeneficio');
            },
						
						/**
             * Atera um benefício de Plano Odontológico de um agregado 'index' ou titular
             * @param {number} index Índice do Agregado, null para titular
             * @param {boolean} ativado Caso verdadeiro ativa o benefício
						 * @param {boolean} titular Define se o benefício é para o titular
             */
            setBeneficioPo: function (index, ativado, titular) {
								if(titular){
									this.FormDataBeneficio.titular_po = ativado;
								}else{
									angular.forEach(this.FormDataBeneficio.agregado_po, function (value, key) {
											if (key === (index - 1)) {
													value = ativado;
											}
									});
								}
                return $rootScope.$broadcast('DadosService.setBeneficioPo');
            },

            /**
             * Atera um benefício de Plano de saúde de um agregado 'index'
             * @param {number} index Índice do Agregado, null para titular
             * @param {boolean} ativado Caso verdadeiro ativa o benefício
						 * @param {boolean} titular Define se o benefício é para o titular
             */
            setBeneficioPs: function (index, ativado, titular) {
								if(titular){
									this.FormDataBeneficio.titular_ps = ativado;
								}else{
									angular.forEach(this.FormDataBeneficio.agregado_ps, function (value, key) {
											if (key === (index - 1)) {
													value = ativado;
											}
									});
								}
                return $rootScope.$broadcast('DadosService.setBeneficioPs');
            },

            testeDeValores: function () {
                this.FormDataTitular = {
                    titular_nome: 'Testonildo Farchibule',
                    titular_datanasc: '1987-08-04',
                    titular_sexo: 'm',
                    titular_naturalidade: 'Porto Velho',
                    titular_situacao: 'servidor_ativo',
                    titular_identidade: '23322233',
                    titular_identidade_oe: 'SSP RO',
                    titular_cpfmf: '987654321',
                    titular_pispasep: '478292847832',
                    titular_cns: '778473939',
                    titular_cargo: 'Servidor Público',
                    titular_dataadmissao: '2002-10-19',
                    titular_secretaria: 'Fazenda',
                    titular_matricula: '99849',
                    titular_estadocivil: 'aposentado',
                    titular_end_logradouro: 'Rua Japirana',
                    titular_end_numero: '1645',
                    titular_end_bairro: 'Jertrudes',
                    titular_end_municipio: 'Porto Velho',
                    titular_end_cep: '76530000',
                    titular_tel_trabalho: '12349090',
                    titular_tel_residencia: '32220022',
                    titular_celular: '00001111',
                    titular_email: 'meuemail@dominio.com',
                    titular_banco_nome: 'banco do brasil',
                    titular_banco_agencia: '885784',
                    titular_banco_cc: '89884'
                };
							
							service.addAgregado({
									agregado_nome: 'Jordana Filho',
									agregado_cpfmf: '87668718249',
									agregado_sexo: 'f',
									agregado_vinculoparentesco: 'Filha',
									agregado_filiacao: 'Valter maio Juci',
									agregado_datanasc: '1998-05-09',
									agregado_cns: '99859585958'
							});
							
							service.addAgregado({
									agregado_nome: 'Jabulani',
									agregado_cpfmf: '87668718249',
									agregado_sexo: 'f',
									agregado_vinculoparentesco: 'Filha',
									agregado_filiacao: 'Valter maio Juci',
									agregado_datanasc: '1998-05-09',
									agregado_cns: 'null'
							});
							
							service.addAgregado({
									agregado_nome: 'Berto Beroti',
									agregado_cpfmf: '87668718249',
									agregado_sexo: 'f',
									agregado_vinculoparentesco: 'Filha',
									agregado_filiacao: 'Valter maio Juci',
									agregado_datanasc: '1998-05-09',
									agregado_cns: '99859585958'
							});
							
							service.addAgregado({
									agregado_nome: 'Subest only',
									agregado_cpfmf: '87668718249',
									agregado_sexo: 'f',
									agregado_vinculoparentesco: 'Filha',
									agregado_filiacao: 'Valter maio Juci',
									agregado_datanasc: '1998-05-09',
									agregado_cns: '99859585958'
							});
							
							service.addAgregado({
									agregado_nome: 'Outro nome',
									agregado_cpfmf: '87668718249',
									agregado_sexo: 'f',
									agregado_vinculoparentesco: 'Filha',
									agregado_filiacao: 'Valter maio Juci',
									agregado_datanasc: '1998-05-09',
									agregado_cns: '99859585958'
							});
							
							service.setBeneficio({
									titular_ps: true,
									titular_po: true,
									agregado_ps: [null,false,false,false,false,false],
									agregado_po: [null,false,false,false,false,false],
									obs: ''
							});

            }
        };
        return service;
    }])
;

app.controller("AdesaoCtrl", ["$window","$scope", "$location", "$anchorScroll", "DadosService",
    function ($window,$scope, $location, $anchorScroll, DadosService) {

        $scope.panel = 0;
        $scope.agregados = DadosService.getAgregados();
        $scope.FormDataTitular = {};
        //$scope.FormDataBeneficio = {};
        $scope.FormDataBeneficio = DadosService.getBeneficio();

        /**
         * Vai par o painel de indice 'index'.
         * @param `{number} index Índice do painel
         */
        $scope.goTo = function (index) {
            $scope.panel = index;
        };

        /**
         * Verifica se o painel atual é de índice 'status'
         * @param `{number} status Índice do painel a ser verificado
         */
        $scope.checkStatus = function (status) {
            if (status <= $scope.panel) {
                return true;
            }
            return false;
        };

        $scope.returnStatus = function (status) {
            if (status == $scope.panel) {
                return true;
            }
            return false;
        }

        /**
         * Navega para o painel anterior.
         */
        $scope.voltar = function () {
            if ($scope.panel > 0) {
                $scope.panel = $scope.panel - 1;
            }
        };

        /**
         * Navega para o próximo painel.
         */
        $scope.continuar = function () {
            if ($scope.panel < 4) {
                $scope.panel = $scope.panel + 1;
            }
        };

        /**
         * Navega para o próximo painel e prepara os dados para impressão.
         */
        $scope.concordar = function () {
            $scope.continuar();
        };

        $scope.imprimir = function(){
          $window.print();
          //  window.print();
        };


        $scope.testar = DadosService.testeDeValores;

        $scope.$on('DadosService.setAgregado', function () {
            $scope.agregados = DadosService.getAgregados();
        });

        $scope.$on('DadosService.addAgregado', function () {
            $scope.agregados = DadosService.getAgregados();
        });
				
				$scope.$on('DadosService.setBeneficio', function () {
            $scope.FormDataBeneficio = DadosService.getBeneficio();
        });
        $scope.$watch('FormDataTitular', function (newValue) {
            DadosService.setTitular(newValue);
            $scope.FormDataTitular = DadosService.getTitular();
        });
        
				$scope.$watch('FormDataBeneficio', function (newValue) {
            DadosService.setBeneficio(newValue);
            $scope.FormDataBeneficio = DadosService.getBeneficio();
        });

    }]);

app.directive('mvAgregado', ['DadosService',
    function (DadosService) {
        return {
            restrict: 'EA',
            templateUrl: 'core/views/directives/agregado.html',
            scope: {
                index: '=?'
            },
            controller: function ($scope, $element, $attrs) {
                $scope.FormDataAgregado = {};

                $scope.$watch('FormDataAgregado', function (newValue, oldValue) {
                    DadosService.setAgregado($scope.index, newValue);
                }, true);
            },
            link: function (scope, element, attrs) {
                DadosService.addAgregado(scope.FormDataAgregado);
            }
        };
    }]);

app.directive('mvBtAgregado', ['$compile', 'DadosService',
    function ($compile, DadosService) {
        return {
            restrict: 'EA',
            template: 'Adicionar dependente e/ou agregado',
            controller: 'AdesaoCtrl',
            link: function (scope, element, attrs) {
                scope.viewAddButton = true;

                var list = angular.element(document.getElementById('listaAgregado'));

                scope.adicionarDependente = function () {
                    var index = DadosService.getTotalAgregados() + 1;
                    if (index < 6) {
                        var newElement = $compile('<li mv-agregado data-agregados="agregados" data-index="' + index + '"></li>')(scope);
                        list.append(newElement);
                    }
                    if (index == 5) {
                        scope.viewAddButton = false;
                    }
                };
            }
        };
    }]);

app.directive('mvRelatorio', ['DadosService',
    function (DadosService) {
        return{
            restrict: 'EA',
            templateUrl: 'core/views/directives/relatorio.html',
            controller: function ($scope, $element, $attrs) {
                $scope.FormDataTitular = DadosService.getTitular();
                $scope.agregados = DadosService.getAgregados();
            },
            link: function (scope, element, attrs) {

            }
        };

    }]);

app.directive('mvResultado', function () {
    return {
        restrict: 'EA',
        template: '<span></span>',
        transclude: true,
        scope: {
            value: '@'
        },
        link: function (scope, element, attrs) {
            var text = attrs.value;
            if (!angular.isObject(attrs.value)) {
                text = 'Não informado'
            }
            element.text(text);
        }
    };
});


app.directive('mvTermos',
    function () {
        return{
            restrict: 'EA',
            templateUrl: 'core/views/directives/termos.html'
        };
    });

app.directive('mvPrintInfo', ['DadosService',
    function (DadosService) {
        return{
            restrict: 'EA',
            templateUrl: 'core/views/directives/informacoes.html'
        };

    }]);