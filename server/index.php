<?php 
$configFile = '../wp-config.php';
$mensagemErro = null;

if(file_exists($configFile)){
	include $configFile;
	$con = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die('Sistema fora do ar. ERR_354676a');
	$selectdb = mysql_select_db(DB_NAME) or die('Sistema fora do ar. ERR_354676a');

	$cpf = (isset($_POST['criterio'])) ? $_POST['criterio'] : 'nada';
	$acao = (isset($_POST['acao'])) ? $_POST['acao'] : 'nada';
}else{
	$mensagemErro = '<p>Sistema de pesquisa de cartão não está habilitado. Volte novamente mais tarte.</p>';
}

?><!doctype html>
<html lang="pt_BR">
<head>
	<meta charset="UTF-8">
	<title>ASPER</title>
	<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<style>
	*{margin: 0;padding: 0;}
	html, body{font-family: "Arial", Helvetica, sans; font-size: 14px;}
	#tudo{margin:0 auto;width:300px; text-align: center;}
	#mainHeader{margin-bottom:1em; min-height: 50px; text-align: center; font-family: 'Dosis', sans-serif; color: #fff; background-color: #ed3438;}
	#mainFooter{padding:2em 0 0 0 ;}
	#resultado{margin-top:2em;}
	#resultado table{margin-top:1em; text-align: center;}
	#resultado table thead{}
	#resultado table thead th{border-top: 2px solid #333;border-bottom: 2px solid #333; line-height: 2em}
	#resultado table tbody{}
	#resultado table tbody td{line-height: 2em;}
	p{ line-height: 1.4em; }
	p.carteira{padding:2em; color:#333; background-color: #bbb; border-bottom: 2px solid #fff;}
	form{border: 1px solid #ed3438;}
	input#inputCriterio,input#btPesquisar{display:block; height:30px; width:300px; padding:1% 0; border:0; }
	input#inputCriterio{width:99%; background-color: #fff; text-align: center;}
	input#btPesquisar{color:#fff; background-color: #ed3438;}
	</style>
</head>
<body>
	<header id="mainHeader">
		<h1>Consulte o número do seu cartão</h1>
	</header>
	<div id="tudo">
	

	<section id="mainContent">
		<p>Digite seu CPF</p>
		<form action="./" id="consultaBeneficio" method="post">
			<input type="text" name="criterio" id="inputCriterio">
			<input type="submit" name="acao" id="btPesquisar" value="Pesquisar">
		</form>

		<?php 
		if(isset($_POST) && $cpf != 'nada' && $acao == 'Pesquisar'):
			$sql = sprintf('SELECT cpf,num_cart FROM cartaobeneficiarios WHERE cpf=\'%s\'',mysql_real_escape_string($cpf));
			$query = mysql_query($sql);
		?><section id="resultado">
						<p>Este é o seu número de usuário, anote-o para utilizá-lo sempre que necessário. Resultado(s) encontrado(s):</p>
			<?php while ($resultado = mysql_fetch_array($query)) :?>
			<p class="carteira"><?php echo $resultado['num_cart']; ?></p>
			<?php endwhile; ?>
		</section>
		<?php endif ?>
	</section>
	

	
	<footer id="mainFooter">
		<p><?php echo date('Y'); ?> . Assossiação dos Trabalhadores no Serviço Público no Estado de Rondônia</p>
	</footer>
	</div>
</body>
</html>